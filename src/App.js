import image from './assets/images/48.jpg';

function App() {
  return (
    <div>
    <div style={ { width: "800px" , margin: "100px auto", textAlign:"center", border:"1px solid #ddd", paddingBottom:"50px" } }>
      <div>
        <img src={image} alt="avatar" style={{ borderRadius:"50%", width:"100px", marginTop:"-50px" }}></img>
      </div>
      <div>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div style={{fontSize:"12px"}}>
        <b>Tammy Stevens</b> &nbsp; * &nbsp; Front End Developer
      </div>
    </div>
  </div>

  );
}

export default App;
